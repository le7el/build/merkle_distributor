import chai, { expect } from 'chai'
import { solidity, MockProvider, deployContract } from 'ethereum-waffle'
import { Contract, BigNumber, utils } from 'ethers'
import { signTicket, prepareOffchainClaim } from '../src/oracles/one-time-offchain-tickets'

import Distributor from '../artifacts/contracts/ConditionalDistributor.sol/ConditionalDistributor.json'
import ERC721Holder from '../artifacts/contracts/condition_oracles/ERC721Holder.sol/ERC721Holder.json'
import OneTimeOffchainTickets from '../artifacts/contracts/condition_oracles/OneTimeOffchainTickets.sol/OneTimeOffchainTickets.json'
import MockERC20 from '../artifacts/contracts/mock/MockERC20.sol/MockERC20.json'
import MockERC721 from '../artifacts/contracts/mock/MockERC721.sol/MockERC721.json'
import MockERC1155 from '../artifacts/contracts/mock/MockERC1155.sol/MockERC1155.json'

chai.use(solidity)

const overrides = {
  gasLimit: 9999999,
}

const ZERO_BYTES32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'
const ERC20_INTERFACE = '0xffffffff'
const ERC20_MINT_INTERFACE = '0x40c10f19'
const ERC1155_MINT_INTERFACE = '0x731133e9'
const ERC1155_INTERFACE = '0xd9b67a26'

describe('ConditionalDistributor', () => {
  const provider = new MockProvider({
    ganacheOptions: {
      hardfork: 'istanbul',
      mnemonic: 'horn horn horn horn horn horn horn horn horn horn horn horn',
      gasLimit: 9999999,
    },
  })

  const wallets = provider.getWallets()
  const [wallet0, wallet1] = wallets

  let token: Contract
  let nft: Contract
  let oracle: Contract
  beforeEach('deploy tokens', async () => {
    nft = await deployContract(wallet0, MockERC721, ['Avatar NFT', 'NFT'], overrides)
    token = await deployContract(wallet0, MockERC20, ['Token', 'TKN', 0], overrides)
    oracle = await deployContract(wallet0, ERC721Holder, [nft.address, ERC20_MINT_INTERFACE, token.address, 0, 0, ZERO_ADDRESS], overrides)
    await oracle.adminSetDefaultReward(BigNumber.from(3000))
  })

  describe('#authorize oracle', () => {
    it('returns true for default oracle', async () => {
      const distributor = await deployContract(wallet0, Distributor, [oracle.address], overrides)
      expect(await distributor.authorizedOracles(oracle.address)).to.eq(true)
    })

    it('returns true for authorized oracle', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      await distributor.adminSwitchOracle(oracle.address, true)
      expect(await distributor.authorizedOracles(oracle.address)).to.eq(true)
    })

    it('returns false for unauthorized oracle', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      expect(await distributor.authorizedOracles(oracle.address)).to.eq(false)
    })

    it('supports switching oracle off', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      await distributor.adminSwitchOracle(oracle.address, true)
      expect(await distributor.authorizedOracles(oracle.address)).to.eq(true)
      await distributor.adminSwitchOracle(oracle.address, false)
      expect(await distributor.authorizedOracles(oracle.address)).to.eq(false)
    })

    it('decline request from non-owner', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      await expect(distributor.connect(wallet1).adminSwitchOracle(oracle.address, true)).to.be.revertedWith(
        'Ownable: caller is not the owner'
      )
    })
  })

  describe('#withdraw rewards', () => {
    it('decline request from non-owner', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      await expect(distributor.connect(wallet1).adminWithdrawUnclaimed(wallet1.address, ERC20_INTERFACE, token.address, 0)).to.be.revertedWith(
        'Ownable: caller is not the owner'
      )
    })

    it('withdraw ERC20 tokens', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      await token.mint(distributor.address, BigNumber.from(10000))
      const bal1 = await token.balanceOf(wallet1.address)
      expect(await token.balanceOf(distributor.address)).to.eq(BigNumber.from(10000))
      await distributor.adminWithdrawUnclaimed(wallet1.address, ERC20_INTERFACE, token.address, 0)
      expect(await token.balanceOf(distributor.address)).to.eq(BigNumber.from(0))
      expect(await token.balanceOf(wallet1.address)).to.eq(bal1.add(BigNumber.from(10000)))
    })

    it('withdraw ERC1155 tokens', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      token = await deployContract(wallet0, MockERC1155, [], overrides)
      await token.mint(distributor.address, 1, BigNumber.from(11000), "0x")
      const bal1 = await token.balanceOf(wallet1.address, 1)
      expect(await token.balanceOf(distributor.address, 1)).to.eq(BigNumber.from(11000))
      await distributor.adminWithdrawUnclaimed(wallet1.address, ERC1155_INTERFACE, token.address, 1)
      expect(await token.balanceOf(distributor.address, 1)).to.eq(BigNumber.from(0))
      expect(await token.balanceOf(wallet1.address, 1)).to.eq(bal1.add(BigNumber.from(11000)))
    })
  })

  describe('#claims', () => {
    it('not possible if paused', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      await distributor.adminSwitchOracle(oracle.address, true)
      await distributor.adminSetPaused(true)
      await nft.connect(wallet1).mint(wallet1.address, 2)
      const claim = await oracle.prepareClaim(ERC20_MINT_INTERFACE, token.address, 0, 2)
      await expect(distributor.claim(wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim)).to.be.revertedWith(
        'Pausable: paused'
      )
    })

    it('get a default reward', async () => {
      const distributor = await deployContract(wallet0, Distributor, [oracle.address], overrides)
      await oracle.adminSwitchConsumer(distributor.address, true)
      await nft.connect(wallet1).mint(wallet1.address, 2)
      const claim = await oracle.prepareClaim(ERC20_MINT_INTERFACE, token.address, 0, 2)
      await distributor.claim(wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim)
      expect(await token.balanceOf(wallet1.address)).to.eq(BigNumber.from(3000))
    })

    it('claim only once', async () => {
      const distributor = await deployContract(wallet0, Distributor, [oracle.address], overrides)
      await oracle.adminSwitchConsumer(distributor.address, true)
      await nft.connect(wallet1).mint(wallet1.address, 2)
      const claim = await oracle.prepareClaim(ERC20_MINT_INTERFACE, token.address, 0, 2)
      expect(await distributor.connect(wallet1).isClaimed(oracle.address, claim)).to.eq(false)
      await distributor.claim(wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim)
      expect(await distributor.connect(wallet1).isClaimed(oracle.address, claim)).to.eq(true)
      await expect(distributor.claim(wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim)).to.be.revertedWith(
        'no claim'
      )
      expect(await token.balanceOf(wallet1.address)).to.eq(BigNumber.from(3000))
    })

    it('not your NFT', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      await distributor.adminSwitchOracle(oracle.address, true)
      await oracle.adminSwitchConsumer(distributor.address, true)
      await nft.connect(wallet0).mint(wallet0.address, 1)
      await nft.connect(wallet1).mint(wallet1.address, 2)
      const claim = await oracle.prepareClaim(ERC20_MINT_INTERFACE, token.address, 0, 1)
      await expect(distributor.claim(wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim)).to.be.revertedWith(
        'not an owner'
      )
      expect(await token.balanceOf(wallet1.address)).to.eq(BigNumber.from(0))
    })

    it('invalid claim interface', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      await distributor.adminSwitchOracle(oracle.address, true)
      await oracle.adminSwitchConsumer(distributor.address, true)
      await nft.connect(wallet1).mint(wallet1.address, 2)
      const claim = await oracle.prepareClaim(ERC20_INTERFACE, token.address, 0, 2)
      await expect(distributor.claim(wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim)).to.be.revertedWith(
        'invalid reward data'
      )
      expect(await token.balanceOf(wallet1.address)).to.eq(BigNumber.from(0))
    })

    it('invalid oracle', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      await nft.connect(wallet1).mint(wallet1.address, 2)
      const claim = await oracle.prepareClaim(ERC20_MINT_INTERFACE, token.address, 0, 2)
      await expect(distributor.claim(wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim)).to.be.revertedWith(
        'invalid oracle'
      )
      expect(await token.balanceOf(wallet1.address)).to.eq(BigNumber.from(0))
    })

    it('invalid consumer', async () => {
      const distributor = await deployContract(wallet0, Distributor, [oracle.address], overrides)
      await nft.connect(wallet1).mint(wallet1.address, 2)
      const claim = await oracle.prepareClaim(ERC20_MINT_INTERFACE, token.address, 0, 2)
      await expect(distributor.claim(wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim)).to.be.revertedWith(
        'not a consumer'
      )
    })

    it('batch several claims', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      await distributor.adminSwitchOracle(oracle.address, true)
      await oracle.adminSwitchConsumer(distributor.address, true)
      await nft.connect(wallet1).mint(wallet1.address, 2)
      await nft.connect(wallet1).mint(wallet1.address, 3)
      const claim1 = await oracle.prepareClaim(ERC20_MINT_INTERFACE, token.address, 0, 2)
      const claim2 = await oracle.prepareClaim(ERC20_MINT_INTERFACE, token.address, 0, 3)
      let inter = new utils.Interface(Distributor.abi);
      const call1 = inter.encodeFunctionData("claim", [wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim1])
      const call2 = inter.encodeFunctionData("claim", [wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim2])
      await distributor.multicall([call1, call2])
      expect(await token.balanceOf(wallet1.address)).to.eq(BigNumber.from(6000))
    })
  })

  describe('#ERC1155 rewards', () => {
    const TOKEN_ID = 1

    beforeEach('deploy ERC1155 token and oracle', async () => {
      token = await deployContract(wallet0, MockERC1155, [], overrides)
      oracle = await deployContract(wallet0, ERC721Holder, [nft.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, BigNumber.from(2000), ZERO_ADDRESS], overrides)
    })

    it('get a default reward with a mint interface', async () => {
      const distributor = await deployContract(wallet0, Distributor, [oracle.address], overrides)
      await oracle.adminSwitchConsumer(distributor.address, true)
      await nft.connect(wallet1).mint(wallet1.address, 2)
      const claim = await oracle.prepareClaim(ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, 2)
      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(2000))
    })

    it('get a default reward with a transfer interface', async () => {
      const distributor = await deployContract(wallet0, Distributor, [ZERO_ADDRESS], overrides)
      oracle = await deployContract(wallet0, ERC721Holder, [nft.address, ERC1155_INTERFACE, token.address, TOKEN_ID, BigNumber.from(3000), distributor.address], overrides)
      await distributor.adminSwitchOracle(oracle.address, true)
      await token.mint(distributor.address, TOKEN_ID, BigNumber.from(3000), "0x")
      await nft.connect(wallet1).mint(wallet1.address, 2)
      const claim = await oracle.prepareClaim(ERC1155_INTERFACE, token.address, TOKEN_ID, 2)
      await distributor.claim(wallet1.address, oracle.address, ERC1155_INTERFACE, token.address, TOKEN_ID, claim)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(3000))
      expect(await token.balanceOf(distributor.address, TOKEN_ID)).to.eq(BigNumber.from(0))
    })
  })

  describe('#ERC721Holder administration', () => {
    const TOKEN_ID = 2

    it('should support reward for specific NFT id', async () => {
      token = await deployContract(wallet0, MockERC1155, [], overrides)
      oracle = await deployContract(wallet0, ERC721Holder, [nft.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, 0, ZERO_ADDRESS], overrides)
      await oracle.adminSetDefaultReward(BigNumber.from(1000))
      await oracle.adminSetReward(BigNumber.from(2000), [2, 3])
      await oracle.adminSetReward(BigNumber.from(0), [1])
      const distributor = await deployContract(wallet0, Distributor, [oracle.address], overrides)
      await oracle.adminSwitchConsumer(distributor.address, true)

      await nft.connect(wallet1).mint(wallet1.address, 3)
      let claim = await oracle.prepareClaim(ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, 3)
      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(2000))

      await nft.connect(wallet1).mint(wallet1.address, 4)
      claim = await oracle.prepareClaim(ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, 4)
      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(3000))

      await nft.connect(wallet1).mint(wallet1.address, 1)
      claim = await oracle.prepareClaim(ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, 1)
      await expect(distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim)).to.be.revertedWith(
        'no claim'
      )
    })

    it('should support change of default reward', async () => {
      token = await deployContract(wallet0, MockERC1155, [], overrides)
      oracle = await deployContract(wallet0, ERC721Holder, [nft.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, 0, ZERO_ADDRESS], overrides)
      await oracle.adminSetDefaultReward(BigNumber.from(1000))
      const distributor = await deployContract(wallet0, Distributor, [oracle.address], overrides)
      await oracle.adminSwitchConsumer(distributor.address, true)
      
      await nft.connect(wallet1).mint(wallet1.address, 3)
      let claim = await oracle.prepareClaim(ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, 3)
      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(1000))
      
      await oracle.adminSetDefaultReward(BigNumber.from(2000))
      await nft.connect(wallet1).mint(wallet1.address, 4)
      claim = await oracle.prepareClaim(ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, 4)
      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(3000))
    })

    it('should support change of settings', async () => {
      token = await deployContract(wallet0, MockERC1155, [], overrides)
      oracle = await deployContract(wallet0, ERC721Holder, [nft.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, 0, ZERO_ADDRESS], overrides)
      await oracle.adminSetDefaultReward(BigNumber.from(1000))
      const distributor = await deployContract(wallet0, Distributor, [oracle.address], overrides)
      await oracle.adminSwitchConsumer(distributor.address, true)
      
      await nft.connect(wallet1).mint(wallet1.address, 3)
      let claim = await oracle.prepareClaim(ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, 3)
      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(1000))
      
      token = await deployContract(wallet0, MockERC20, ['Token', 'TKN', 0], overrides)
      await oracle.adminChangeSettings(nft.address, ERC20_MINT_INTERFACE, token.address, 0)
      await nft.connect(wallet1).mint(wallet1.address, 4)
      claim = await oracle.prepareClaim(ERC20_MINT_INTERFACE, token.address, 0, 4)
      await distributor.claim(wallet1.address, oracle.address, ERC20_MINT_INTERFACE, token.address, 0, claim)
      expect(await token.balanceOf(wallet1.address)).to.eq(BigNumber.from(1000))
    })
  })

  describe('#OneTimeOffchainTickets', () => {
    const TOKEN_ID = 1
    let distributor : Contract
    let domainSeparator : string

    beforeEach('deploy ERC1155 token and oracle', async () => {
      token = await deployContract(wallet0, MockERC1155, [], overrides)
      oracle = await deployContract(wallet0, OneTimeOffchainTickets, [ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, ZERO_ADDRESS], overrides)
      distributor = await deployContract(wallet0, Distributor, [oracle.address], overrides)
      domainSeparator = await oracle.getDomainSeparator()
      await oracle.adminSwitchConsumer(distributor.address, true)
    })

    it('invalid validator', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet1, domainSeparator, wallet1.address, 100, 0, nonce1)
      expect(await oracle.isAllowed(wallet1.address, 100, 0, nonce1, ticket1Sig)).to.eq('0x00000000')
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    }) 

    it('invalid nonce', async () => {
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, 0)
      expect(await oracle.isAllowed(wallet1.address, 100, 0, 0, ticket1Sig)).to.eq('0x834943e8')
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        0, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    })

    it('invalid amount 1', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 0, 0, nonce1)
      expect(await oracle.isAllowed(wallet1.address, 0, 0, nonce1, ticket1Sig)).to.eq('0x834943e8')
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        0,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    })

    it('invalid amount 2', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        200,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.isAllowed(wallet1.address, 200, 0, nonce1, ticket1Sig)).to.eq('0x00000000')
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    })

    it('invalid amount 3', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 100, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.isAllowed(wallet1.address, 100, 0, nonce1, ticket1Sig)).to.eq('0x00000000')
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    })

    it('invalid amount 4', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 100, nonce1)
      expect(await oracle.isAllowed(wallet1.address, 100, 100, nonce1, ticket1Sig)).to.eq('0x834943e8')
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        100,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    })

    it('invalid amount 5', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        100,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.isAllowed(wallet1.address, 100, 100, nonce1, ticket1Sig)).to.eq('0x00000000')
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    })

    it('invalid mix of wallets 1', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet0.address, 0, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet0.address,
        0,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    })

    it('invalid mix of wallets 2', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 0, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet0.address,
        0,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    })

    it('invalid mix of wallets 3', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet0.address, 0, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        0,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    })

    it('invalid integrity', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC20_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )
      await expect(oracle.hasClaim(wallet1.address, claim1)).to.be.revertedWith(
        'invalid claim'
      )
    })

    it('should generate valid claim for one-time ticket', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(true)
    })

    it('should claim with valid claim for one-time ticket, fail otherwise', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )
      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim1)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(100))
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
      await expect(distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim1)).to.be.revertedWith(
        'expired ticket'
      )
    })

    it('should invalidate older or the same time tickets after claim', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )

      const ticket2Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 200, 0, nonce1)
      const claim2 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        200,
        0,
        nonce1, 
        ticket2Sig
      )

      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim2)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(200))
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
      await expect(distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim1)).to.be.revertedWith(
        'expired ticket'
      )
    })

    it('should invalidate tickets issued for a wrong claimed amount', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )

      const ticket2Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 200, 0, nonce1 + 1)
      const claim2 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        200,
        0,
        nonce1 + 1, 
        ticket2Sig
      )

      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim1)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(100))
      expect(await oracle.hasClaim(wallet1.address, claim2)).to.eq(false)
      await expect(distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim2)).to.be.revertedWith(
        'invalid amount'
      )
    })

    it('should fail is called directly, by non-consumer', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )
      await expect(oracle.consumeClaim(wallet1.address, claim1)).to.be.revertedWith(
        'not a consumer'
      )
    })

    it('should allow claim with a sequentially generated and claimed tickets', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )
      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim1)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(100))

      const nonce2 = await oracle.nextNonce(wallet1.address)
      const claimedAmount = await oracle.claimedAmount(wallet1.address)
      const ticket2Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 200, claimedAmount, nonce2)
      const claim2 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        200,
        claimedAmount,
        nonce2, 
        ticket2Sig
      )
      await distributor.claim(wallet1.address, oracle.address, ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, claim2)
      expect(await token.balanceOf(wallet1.address, TOKEN_ID)).to.eq(BigNumber.from(300))
    })
  })

  describe('#OneTimeOffchainTickets administration', () => {
    const TOKEN_ID = 1
    let distributor : Contract
    let domainSeparator : string

    beforeEach('deploy ERC1155 token and oracle', async () => {
      token = await deployContract(wallet0, MockERC1155, [], overrides)
      oracle = await deployContract(wallet0, OneTimeOffchainTickets, [ERC1155_MINT_INTERFACE, token.address, TOKEN_ID, ZERO_ADDRESS], overrides)
      distributor = await deployContract(wallet0, Distributor, [oracle.address], overrides)
      domainSeparator = await oracle.getDomainSeparator()
      await oracle.adminSwitchConsumer(distributor.address, true)
    })

    it('should allow only owner to change settings', async () => {
      token = await deployContract(wallet0, MockERC20, ['Token', 'TKN', 0], overrides)
      await expect(oracle.connect(wallet1).adminChangeSettings(ERC20_MINT_INTERFACE, token.address, 0)).to.be.revertedWith(
        'Ownable: caller is not the owner'
      )
    })

    it('should invalidate tickets issued with the old settings', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      expect(await oracle.isAllowed(wallet1.address, 100, 0, nonce1, ticket1Sig)).to.eq('0x834943e8')
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(true)
      token = await deployContract(wallet0, MockERC20, ['Token', 'TKN', 0], overrides)
      await oracle.adminChangeSettings(ERC20_MINT_INTERFACE, token.address, 0)
      expect(await oracle.isAllowed(wallet1.address, 100, 0, nonce1, ticket1Sig)).to.eq('0x834943e8')
      await expect(oracle.hasClaim(wallet1.address, claim1)).to.be.revertedWith(
        'invalid claim' 
      )

      const claim2 = prepareOffchainClaim(
        ERC20_MINT_INTERFACE,
        token.address,
        0,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim2)).to.eq(true)
    })

    it('should allow only owner to change validator', async () => {
      await expect(oracle.connect(wallet1).adminChangeValidator(wallet1.address)).to.be.revertedWith(
        'Ownable: caller is not the owner' 
      )
    })

    it('should allow only owner to change consumer', async () => {
      await expect(oracle.connect(wallet1).adminSwitchConsumer(distributor.address, false)).to.be.revertedWith(
        'Ownable: caller is not the owner' 
      )
    })

    it('new validator should render previous tickets invalid', async () => {
      const nonce1 = await oracle.nextNonce(wallet1.address)
      const ticket1Sig = await signTicket(wallet0, domainSeparator, wallet1.address, 100, 0, nonce1)
      expect(await oracle.isAllowed(wallet1.address, 100, 0, nonce1, ticket1Sig)).to.eq('0x834943e8')
      const claim1 = prepareOffchainClaim(
        ERC1155_MINT_INTERFACE,
        token.address,
        TOKEN_ID,
        wallet1.address,
        100,
        0,
        nonce1, 
        ticket1Sig
      )
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(true)
      await oracle.adminChangeValidator(wallet1.address)
      expect(await oracle.isAllowed(wallet1.address, 100, 0, nonce1, ticket1Sig)).to.eq('0x00000000')
      expect(await oracle.hasClaim(wallet1.address, claim1)).to.eq(false)
    })
  })
})