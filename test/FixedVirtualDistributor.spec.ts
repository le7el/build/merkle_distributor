import { expect } from 'chai'
import hre from 'hardhat'
import { Contract, Signer } from 'ethers'
import { time } from '@nomicfoundation/hardhat-network-helpers'

const { deployContract } = hre.ethers

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'

describe('FixedVirtualDistributor', () => {
  const wallets = hre.ethers.getSigners()

  let wallet0: Signer
  let wallet1: Signer
  let wallet2: Signer
  let wallet3: Signer
  let wallet4: Signer
  let token: Contract
  let nft: Contract
  let rewarder: Contract
  beforeEach('deploy tokens', async () => {
    [wallet0, wallet1, wallet2, wallet3, wallet4] = await wallets
    nft = await deployContract("MockERC721", ['Contributor NFT', 'NFT'])
    token = await deployContract("MockERC20", ['Experience', 'EXP', 0])
    rewarder = await deployContract("ERC20Rewarder")
  })

  describe('#deployment of reward pool', () => {
    it('not possible to make pool for non-existing NFTs', async () => {
      const ts = await time.latest()
      await expect(deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 100, nft.address, [1, 2, 3], [40, 10, 10]]))
        .to.be.revertedWith('ERC721: invalid token ID')
    })

    it('not possible to make pool for invalid NFT contract', async () => {
      const ts = await time.latest()
      await expect(deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 100, ZERO_ADDRESS, [1, 2, 3], [40, 10, 10]]))
        .to.be.reverted
    })

    it('not possible to make pool for no reward', async () => {
      const ts = await time.latest()
      await expect(deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 0, nft.address, [1, 2, 3], [40, 10, 10]]))
        .to.be.reverted
    })

    it('not possible to make pool with invalid NFT configuration', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      await expect(deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 100, nft.address, [1, 2, 3], [40, 10]]))
        .to.be.reverted
      await expect(deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 100, nft.address, [1, 2, 3], [40, 10, 0]]))
        .to.be.reverted
      await expect(deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 100, nft.address, [1, 2, 5], [40, 10, 0]]))
        .to.be.reverted
    })

    it('not possible to make pool with invalid vesting', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      await expect(deployContract("FixedVirtualDistributor", [ts, ts - 1000, rewarder.address, 100, nft.address, [1, 2, 3], [40, 10, 10]]))
        .to.be.reverted
    })

    it('pool is configured properly', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 100, nft.address, [1, 2, 3], [40, 10, 10]])
      expect(await distributor.totalReward()).to.eq(100)
      expect(await distributor.vestingStarts()).to.eq(ts)
      expect(await distributor.vestingEnds()).to.eq(ts + 1000)
      expect(await distributor.REWARDER()).to.eq(rewarder.address)
      expect(await distributor.NFT_CONTRACT()).to.eq(nft.address)
      await expect(await distributor.pendingRewards(1)).to.eq(0)
      await expect(await distributor.pendingRewards(2)).to.eq(0)
      await expect(await distributor.pendingRewards(3)).to.eq(0)
      await expect(rewarder.connect(wallet1).immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0))
        .to.be.revertedWithCustomError(rewarder, "Unauthorized")
      await expect(rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0))
      await expect(rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0))
        .to.be.revertedWithCustomError(rewarder, "Unauthorized")
    })
  })

  describe('#pending rewards', () => {
    it('when vesting not started', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      await nft.connect(wallet4).mint(wallet4.getAddress(), 4)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts + 200, ts + 1200, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)
      await time.increaseTo(ts + 50)
      await expect(await distributor.pendingRewards(1)).to.eq(0)
      await time.increaseTo(ts + 100)
      await expect(await distributor.pendingRewards(1)).to.eq(0)
      await expect(await distributor.pendingRewards(2)).to.eq(0)
      await expect(await distributor.pendingRewards(3)).to.eq(0)
      await expect(await distributor.pendingRewards(4)).to.eq(0)
    })
    
    it('proper simple rewards numbers', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      await nft.connect(wallet4).mint(wallet4.getAddress(), 4)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)
      await expect(await distributor.pendingRewards(1)).to.eq(0)
      await expect(await distributor.pendingRewards(2)).to.eq(0)
      await expect(await distributor.pendingRewards(3)).to.eq(0)
      await time.increaseTo(ts + 100)
      await expect(await distributor.pendingRewards(1)).to.eq(40)
      await expect(await distributor.pendingRewards(2)).to.eq(10)
      await expect(await distributor.pendingRewards(3)).to.eq(10)
      await expect(await distributor.pendingRewards(4)).to.eq(0)
      // expected preciion loss
      await time.increaseTo(ts + 111)
      await expect(await distributor.pendingRewards(1)).to.eq(44)
      await expect(await distributor.pendingRewards(2)).to.eq(11)
      await expect(await distributor.pendingRewards(3)).to.eq(11)
      await expect(await distributor.pendingRewards(4)).to.eq(0)
      await time.increaseTo(ts + 1111)
      await expect(await distributor.pendingRewards(1)).to.eq(400)
      await expect(await distributor.pendingRewards(2)).to.eq(100)
      await expect(await distributor.pendingRewards(3)).to.eq(100)
      await expect(await distributor.pendingRewards(4)).to.eq(0)
    })

    it('proper rewards numbers with penalties and claims', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      await nft.connect(wallet4).mint(wallet4.getAddress(), 4)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts + 100, ts + 1100, rewarder.address, 6000, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)
      await time.increaseTo(ts + 100)
      await expect(await distributor.pendingRewards(1)).to.eq(0)
      await expect(await distributor.pendingRewards(2)).to.eq(0)
      await expect(await distributor.pendingRewards(3)).to.eq(0)
      await time.increaseTo(ts + 200)
      await expect(await distributor.pendingRewards(1)).to.eq(400)
      await expect(await distributor.pendingRewards(2)).to.eq(100)
      await expect(await distributor.pendingRewards(3)).to.eq(100)
      await expect(await distributor.pendingRewards(4)).to.eq(0)
      await distributor.adminPenaltyAdd(3)
      await time.increaseTo(ts + 300)
      await expect(await distributor.pendingRewards(1)).to.within(835, 836)
      await expect(await distributor.pendingRewards(2)).to.within(208, 209)
      await expect(await distributor.pendingRewards(3)).to.within(154, 155)
      await expect(await distributor.pendingRewards(4)).to.eq(0)
      await distributor.adminPenaltyRemove(3)
      await time.increaseTo(ts + 400)
      await expect(await distributor.pendingRewards(1)).to.within(1236, 1237)
      await expect(await distributor.pendingRewards(2)).to.eq(309)
      await expect(await distributor.pendingRewards(3)).to.within(254, 255)
      await expect(await distributor.pendingRewards(4)).to.eq(0)
      await distributor.connect(wallet3).claim(3, 100)
      await expect(await distributor.pendingRewards(3)).to.within(154, 155)
    })
  })

  describe('#admin controls', () => {
    it('unauthorized users cant use admin controls', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)
      await expect(distributor.connect(wallet1).adminPenaltyAdd(3))
        .to.be.revertedWith('Ownable: caller is not the owner')
      await distributor.adminPenaltyAdd(3)
      await expect(distributor.connect(wallet1).adminPenaltyRemove(3))
        .to.be.revertedWith('Ownable: caller is not the owner')
      await time.increase(100)
      await distributor.adminPenaltyRemove(3)
    })

    it('cant penalise the same user 2 times', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)
      await expect(distributor.connect(wallet0).adminPenaltyAdd(3))
      await time.increase(100)
      await expect(distributor.connect(wallet0).adminPenaltyAdd(3))
        .to.be.revertedWithCustomError(distributor, "InvalidNFT")
    })

    it('cant penalise users who are not in a pool', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)
      await expect(distributor.connect(wallet0).adminPenaltyAdd(4))
        .to.be.revertedWithCustomError(distributor, "InvalidNFT")
    })

    it('cant remove penalty if not penalty', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)

      await expect(distributor.connect(wallet0).adminPenaltyRemove(3))
        .to.be.revertedWithCustomError(distributor, "InvalidNFT")
    })
  })

  describe('#claims', () => {
    it('unauthorized users dont have any claims', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      await nft.connect(wallet4).mint(wallet4.getAddress(), 4)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)

      await time.increase(100)
      await expect(distributor.connect(wallet4).claim(4, 1))
        .to.be.revertedWithCustomError(distributor, "NothingToClaim")
    })

    it('unauthorized users cant claim for other', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      await nft.connect(wallet4).mint(wallet4.getAddress(), 4)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)

      await time.increase(100)
      await expect(distributor.connect(wallet4).claim(1, 1))
        .to.be.revertedWithCustomError(distributor, "Unauthorized")
    })

    it('authorized users can claim', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      await nft.connect(wallet4).mint(wallet4.getAddress(), 4)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)

      await time.increaseTo(ts + 100)
      await expect(distributor.connect(wallet1).claim(1, 1))
        .to.emit(distributor, "Claimed")
        .withArgs(1, await wallet1.getAddress(), 1)
      await expect(distributor.connect(wallet1).claim(1, 3))
        .to.emit(distributor, "Claimed")
        .withArgs(1, await wallet1.getAddress(), 3)
      await expect(await token.balanceOf(wallet1.getAddress())).to.eq(4)
      await expect(distributor.connect(wallet1).claim(1, 0))
        .to.emit(distributor, "Claimed")
        .withArgs(1, await wallet1.getAddress(), 36)
      await expect(await token.balanceOf(wallet1.getAddress())).to.eq(40)
    })

    it('authorized users cant claim more than allocated', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      await nft.connect(wallet4).mint(wallet4.getAddress(), 4)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)
      
      await time.increaseTo(ts + 100)
      await expect(distributor.connect(wallet1).claim(1, 41))
        .to.be.reverted
      await expect(distributor.connect(wallet1).claim(1, 39))
      await expect(await token.balanceOf(wallet1.getAddress())).to.eq(39)
      await expect(distributor.connect(wallet1).claim(1, 2))
        .to.be.revertedWithCustomError(distributor, "NotVested")
    })
  })

  describe('#rewarder', () => {
    it('should be configured properly', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])

      await expect(rewarder.immutableConfig(ZERO_ADDRESS, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0))
        .to.be.revertedWithCustomError(rewarder, "InvalidConfig")
      await expect(rewarder.immutableConfig(token.address, ZERO_ADDRESS, ZERO_ADDRESS, 0x40c10f19, token.address, 0))
        .to.be.revertedWithCustomError(rewarder, "InvalidConfig")
      await expect(rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, ZERO_ADDRESS, 0))
        .to.be.revertedWithCustomError(rewarder, "InvalidConfig")
      await expect(rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0xffffffff, token.address, 0))
        .to.be.revertedWithCustomError(rewarder, "InvalidConfig")
      await expect(rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0xffffffff, ZERO_ADDRESS, 100))
        .to.be.revertedWithCustomError(rewarder, "InvalidConfig")
      await expect(rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x11111111, token.address, 0))
        .to.be.revertedWithCustomError(rewarder, "InvalidConfig")
    })

    it('should authorize caller once', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)
      await expect(rewarder.immutableConfig(token.address, await wallet1.getAddress(), ZERO_ADDRESS, 0x40c10f19, token.address, 0))
        .to.be.revertedWithCustomError(rewarder, "Unauthorized")
      await expect(rewarder.immutableConfig(token.address, distributor.address, await wallet1.getAddress(), 0x40c10f19, token.address, 0))
        .to.be.revertedWithCustomError(rewarder, "Unauthorized")
    })
    
    it('shouldnt be called directly', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 0)
      await time.increaseTo(ts + 100)
      await expect(rewarder.connect(wallet1).onReward(nft.address, 1, await wallet1.getAddress(), await wallet1.getAddress(), 40, 40))
        .to.be.revertedWithCustomError(rewarder, "Unauthorized")
    })
    
    it('should be authorized by rewarder', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await time.increaseTo(ts + 100)
      await expect(distributor.connect(wallet1).claim(1, 1))
        .to.be.revertedWithCustomError(rewarder, "Unauthorized")
    })

    it('should support transfering of ERC20 instead of minting', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0xffffffff, ZERO_ADDRESS, 0)

      await time.increaseTo(ts + 100)
      await expect(distributor.connect(wallet1).claim(1, 40))
        .to.be.revertedWith("ERC20: transfer amount exceeds balance")
      await token.mint(rewarder.address, 50)
      await expect(distributor.connect(wallet1).claim(1, 40))
      await time.increase(1)
      await expect(await token.balanceOf(rewarder.address)).to.eq(10)
      await expect(await token.balanceOf(await wallet1.getAddress())).to.eq(40)
    })

    it('should return expected values', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, await wallet1.getAddress(), distributor.address, 0x40c10f19, token.address, 0)
      await time.increaseTo(ts + 100)
      const [[wallet], [amount]] = await rewarder.pendingTokens(nft.address, 1, 40)
      await expect(wallet).to.eq(token.address)
      await expect(amount).to.eq(40)
    })

    it('should not exceed minting cap if set', async () => {
      await nft.connect(wallet1).mint(wallet1.getAddress(), 1)
      await nft.connect(wallet2).mint(wallet2.getAddress(), 2)
      await nft.connect(wallet3).mint(wallet3.getAddress(), 3)
      const ts = await time.latest()
      const distributor = await deployContract("FixedVirtualDistributor", [ts, ts + 1000, rewarder.address, 600, nft.address, [1, 2, 3], [40, 10, 10]])
      await rewarder.immutableConfig(token.address, distributor.address, ZERO_ADDRESS, 0x40c10f19, token.address, 30)
      await time.increaseTo(ts + 100)
      await distributor.connect(wallet1).claim(1, 25)
      await expect(distributor.connect(wallet1).claim(1, 10))
        .to.be.revertedWithCustomError(rewarder, "ExceededMintCap")
    })
  });
})