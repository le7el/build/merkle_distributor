import { expect } from 'chai'
import hre from 'hardhat'
import { Contract, Signer, BigNumber } from 'ethers'
import { time } from '@nomicfoundation/hardhat-network-helpers'

const { deployContract } = hre.ethers

const ZERO_BYTES32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'

describe('VirtualDistributor', () => {
  const wallets = hre.ethers.getSigners()

  let wallet0: Signer
  let wallet1: Signer
  let token: Contract
  let nft: Contract
  beforeEach('deploy tokens', async () => {
    [wallet0, wallet1] = await wallets
    nft = await deployContract("MockERC721", ['Avatar NFT', 'NFT'])
    token = await deployContract("MockERC20", ['Experience', 'EXP', 0])
  })

  describe('#pool management', () => {
    it('owner can add a new NFT contract to the reward pool', async () => {
      const distributor = await deployContract("VirtualDistributor", ['1000000000000000000'])
      await expect(distributor.adminAddPool(nft.address, 1, 1000, ZERO_ADDRESS))
        .to.emit(distributor, 'LogPoolAddition')
        .withArgs(nft.address, 1, 1000, ZERO_ADDRESS)
      expect(await distributor.totalAllocPoint()).to.eq(1)
      expect(await distributor.rewardMultipliers(nft.address)).to.eq(1000)
      const pool = await distributor.poolInfo(nft.address)
      expect(pool.crs).to.eq(await nft.crs())
      expect(pool.project).to.eq(await nft.baseNode())
      expect(pool.rewarder).to.eq(ZERO_ADDRESS)
      expect(pool.virtualTotalSupply).to.eq(0)
      expect(pool.accRewardPerShare).to.eq(0)
      expect(pool.lastRewardBlock).to.eq(await time.latestBlock())
      expect(pool.allocPoint).to.eq(1)

      const nft2 = await deployContract("MockERC721", ['Character NFT', 'CHR'])
      await expect(distributor.adminAddPool(nft2.address, 2, 2000, ZERO_ADDRESS))
        .to.emit(distributor, 'LogPoolAddition')
        .withArgs(nft2.address, 2, 2000, ZERO_ADDRESS)
      expect(await distributor.totalAllocPoint()).to.eq(3)
      expect(await distributor.rewardMultipliers(nft2.address)).to.eq(2000)
    })

    it('only owner can add reward pools', async () => {
      const distributor = await deployContract("VirtualDistributor", ['1000000000000000000'])
      await expect(distributor.connect(wallet1).adminAddPool(nft.address, 1, 1000, ZERO_ADDRESS))
        .to.be.revertedWith(
          'Ownable: caller is not the owner'
        )
    })
  })

  describe('#pool usage', () => {
    let distributor: Contract

    beforeEach('deploy', async () => {
      distributor = await deployContract("VirtualDistributor", ['1000000000000000000'])
      await distributor.adminAddPool(nft.address, 1, 10000, ZERO_ADDRESS)
    })

    it('NFT owner can join rewards program', async () => {
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 2)
      expect(await distributor.connect(wallet1).join(nft.address, 2))
        .to.emit(distributor, 'Dropin')
        .withArgs(await wallet1.getAddress(), nft.address, 2, 1)
    })

    it('cant join rewards program without eligible NFT', async () => {
      const nft2 = await deployContract("MockERC721", ['Fake Avatar NFT', 'NFT'])
      await nft2.connect(wallet1).mint(await wallet1.getAddress(), 2)
      await expect(distributor.connect(wallet1).join(nft2.address, 2))
        .to.be.revertedWith(
          'invalid multiplier'
        )

      await nft.mint(await wallet0.getAddress(), 3)
      await expect(distributor.connect(wallet1).join(nft.address, 3))
        .to.be.revertedWith(
          'not an NFT owner'
        )
    })

    it('double joins can dry run or update level', async () => {
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 2)
      expect(await distributor.connect(wallet1).join(nft.address, 2))
        .to.emit(distributor, 'Dropin')
        .withArgs(await wallet1.getAddress(), nft.address, 2, 1)
      let pool = await distributor.poolInfo(nft.address)
      expect(pool.virtualTotalSupply).to.eq(1)
      expect(pool.accRewardPerShare).to.eq(0)
      expect(pool.lastRewardBlock).to.eq(await time.latestBlock())
      expect(pool.allocPoint).to.eq(1)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq(0)

      expect(await distributor.connect(wallet1).join(nft.address, 2))
        .to.emit(distributor, 'Dropin')
        .withArgs(await wallet1.getAddress(), nft.address, 2, 1)
      await distributor.updatePool(nft.address)
      pool = await distributor.poolInfo(nft.address)
      expect(pool.virtualTotalSupply).to.eq(1)
      // 2 blocks 1 reward per share with 1e23 (precision decimals) + 1e18 (token decimals)
      expect(pool.accRewardPerShare).to.eq(BigNumber.from('200000000000000000000000000000000000000000'))
      expect(pool.lastRewardBlock).to.eq(await time.latestBlock())
      expect(pool.allocPoint).to.eq(1)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('2000000000000000000')

      await nft.setDefaultLevel(3)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('3000000000000000000')
      expect(await distributor.connect(wallet1).join(nft.address, 2))
        .to.emit(distributor, 'Dropin')
        .withArgs(await wallet1.getAddress(), nft.address, 2, 9)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('4000000000000000000')
      await distributor.updatePool(nft.address)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('4999999999999999999')
      pool = await distributor.poolInfo(nft.address)
      expect(pool.virtualTotalSupply).to.eq(9)
      // 4 blocks 1 reward per share plus 1 block 0.11 reward per share because total share amount increased to 10 before the last updatePool call
      expect(pool.accRewardPerShare).to.eq(BigNumber.from('411111111111111111111111111111111111111111'))
      expect(pool.lastRewardBlock).to.eq(await time.latestBlock())
      expect(pool.allocPoint).to.eq(1)
    })

    it('ensure valid allocations for several participants', async () => {
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 2)
      await distributor.connect(wallet1).join(nft.address, 2)
      await nft.setDefaultLevel(3)
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 3)
      await distributor.connect(wallet1).join(nft.address, 3)
      await nft.setDefaultLevel(5)
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 4)
      await distributor.connect(wallet1).join(nft.address, 4)
      await distributor.updatePool(nft.address)
      await nft.setDefaultLevel(2)
      await distributor.connect(wallet1).join(nft.address, 2)
      await distributor.updatePool(nft.address)

      // 3 blocks 100% (3), 3 blocks 10% (0.3), 3 blocks ~2.85% (~0.0857), 1 block ~10.52% (~0.1052)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('3490977443609022556')
      // 3 blocks 0% (0), 3 blocks 90% (2.7), 3 blocks ~25.71% (0.771), 1 block ~23.68% (~0.2368)
      expect(await distributor.pendingRewards(nft.address, 3)).to.eq('3708270676691729323')
      // 6 blocks 0%, 3 blocks ~71.42% (~2.14), 1 block ~65.78% (0.657)
      expect(await distributor.pendingRewards(nft.address, 4)).to.eq('2800751879699248120')
    })
  })

  describe('#harvesting', () => {
    let distributor: Contract
    let rewardToken: Contract

    beforeEach('deploy', async () => {
      rewardToken = await deployContract("MockERC20", ["Reward token", "GOLD", 0])
      let rewarder = await deployContract("MockRewarder", [rewardToken.address])
      distributor = await deployContract("VirtualDistributor", ['1000000000000000000'])
      await distributor.adminAddPool(nft.address, 1, 10000, rewarder.address)
      
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 2)
      await distributor.connect(wallet1).join(nft.address, 2)
      await nft.setDefaultLevel(3)
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 3)
      await distributor.connect(wallet1).join(nft.address, 3)
      await nft.setDefaultLevel(5)
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 4)
      await distributor.connect(wallet1).join(nft.address, 4)
      await distributor.updatePool(nft.address)
      await nft.setDefaultLevel(2)
      await distributor.connect(wallet1).join(nft.address, 2)
      await distributor.updatePool(nft.address)
      // Current stacking allocations: NFT 2 - 3.490, NFT 3 - 3.708, NFT 4 - 2.8
    })

    it('NFT owner can claim unlocked rewards', async () => {
      await distributor.connect(wallet1).unlockHarvest(nft.address, 2)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 3)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 4)
      await time.increase(86400)
      await distributor.connect(wallet1).harvest(nft.address, 2)
      expect(await rewardToken.balanceOf(await wallet1.getAddress())).to.eq('4017293233082706766') // ~0.42 reward for 4 blocks since 3.490
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq(0)
      await distributor.connect(wallet1).join(nft.address, 2)
      await distributor.updatePool(nft.address)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('210526315789473684') // ~0.2105 new reward on NFT, the rest was claimed
      expect(await distributor.pendingRewards(nft.address, 4)).to.eq('7406015037593984962') // ~5.4 reward for 7 blocks since 2.8
    })
  })

  describe('#harvesting with vesting', () => {
    let distributor: Contract
    let rewardToken: Contract
    let rewarder: Contract
    let admin: String

    beforeEach('deploy', async () => {
      admin = await wallet0.getAddress()
      rewardToken = await deployContract("MockERC20", ["Reward token", "GOLD", 0])
      rewarder = await deployContract("VestingRewarder", [admin])
      distributor = await deployContract("VirtualDistributor", ['1000000000000000000'])
      await distributor.adminAddPool(nft.address, 1, 10000, rewarder.address)
      const ts = await time.latest() + 86400 // account unlock period
      const tenMonths = 86400 * 300
      const releaseDelta = 2880000
      await rewarder.immutableConfig(rewardToken.address, distributor.address, ts - releaseDelta, ts + tenMonths)
      await rewardToken.mint(rewarder.address, '10000000000000000000000')
      
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 2)
      await distributor.connect(wallet1).join(nft.address, 2)
      await nft.setDefaultLevel(3)
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 3)
      await distributor.connect(wallet1).join(nft.address, 3)
      await nft.setDefaultLevel(5)
      await nft.connect(wallet1).mint(await wallet1.getAddress(), 4)
      await distributor.connect(wallet1).join(nft.address, 4)
      await distributor.updatePool(nft.address)
      await nft.setDefaultLevel(2)
      await distributor.connect(wallet1).join(nft.address, 2)
      await distributor.updatePool(nft.address)
      // it's expected to reward 0.1052 per block to NFT id 2
    })

    it('should allow only admin to withdraw unclaimed tokens', async () => {
      await expect(rewarder.connect(wallet1).adminWithdrawUnclaimed(admin))
        .to.be.revertedWithCustomError(rewarder, "Unauthorized")
      await expect(await rewardToken.balanceOf(admin)).to.eq('0')
      await rewarder.connect(wallet0).adminWithdrawUnclaimed(admin)
      await expect(await rewardToken.balanceOf(admin)).to.eq('10000000000000000000000')
    })

    it('should enforce correct config', async () => {
      let testRewarder = await deployContract("VestingRewarder", [admin])
      const ts = await time.latest()
      await expect(testRewarder.connect(wallet1).immutableConfig(rewardToken.address, distributor.address, ts, ts + 1000))
        .to.be.revertedWithCustomError(testRewarder, "Unauthorized")
      await expect(testRewarder.connect(wallet0).immutableConfig(rewardToken.address, distributor.address, ts + 1000, ts))
        .to.be.revertedWithCustomError(testRewarder, "InvalidConfig")
      await expect(testRewarder.connect(wallet0).immutableConfig(ZERO_ADDRESS, distributor.address, ts, ts + 1000))
        .to.be.revertedWithCustomError(testRewarder, "InvalidConfig")
      await expect(testRewarder.connect(wallet0).immutableConfig(rewardToken.address, ZERO_ADDRESS, ts, ts + 1000))
        .to.be.revertedWithCustomError(testRewarder, "InvalidConfig")
      await testRewarder.connect(wallet0).immutableConfig(rewardToken.address, distributor.address, ts, ts + 1000)
      expect(await testRewarder.REWARD_TOKEN()).to.eq(rewardToken.address)
      expect(await testRewarder.TRUSTED_AUTHORIZED_CALLER()).to.eq(distributor.address)
      await expect(testRewarder.connect(wallet0).immutableConfig(rewardToken.address, nft.address, ts, ts + 1000))
        .to.be.revertedWithCustomError(testRewarder, "Unauthorized")
    })

    it('should honor not started vesting', async () => {
      let testRewarder = await deployContract("VestingRewarder", [admin])
      const ts = await time.latest()
      await testRewarder.connect(wallet0).immutableConfig(rewardToken.address, distributor.address, ts + 86500, ts + 100000)
      await rewardToken.mint(testRewarder.address, '10000000000000000000000')
      await distributor.adminSetAllocPoint(nft.address, 1, testRewarder.address, true)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 2)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 3)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 4)
      await time.increase(86400)
      await expect(distributor.connect(wallet1).harvest(nft.address, 2))
        .to.be.revertedWithCustomError(testRewarder, "NotVested")
      expect(await testRewarder.claimableAmount(await wallet1.getAddress())).to.eq(0)
      expect(await testRewarder.vestingLedger(await wallet1.getAddress())).to.eq(0)
      expect(await rewardToken.balanceOf(await wallet1.getAddress())).to.eq(0)
      await expect(testRewarder.connect(wallet1).claimVested(await wallet1.getAddress()))
        .to.be.revertedWithCustomError(testRewarder, "NotVested")
    })

    it('should work without vesting', async () => {
      let testRewarder = await deployContract("VestingRewarder", [admin])
      const ts = await time.latest()
      await testRewarder.connect(wallet0).immutableConfig(rewardToken.address, distributor.address, ts, ts + 1000)
      await rewardToken.mint(testRewarder.address, '10000000000000000000000')
      await distributor.adminSetAllocPoint(nft.address, 1, testRewarder.address, true)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 2)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 3)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 4)
      await time.increase(86400) // vesting expired
      await distributor.connect(wallet1).harvest(nft.address, 2)
      expect(await testRewarder.claimableAmount(await wallet1.getAddress())).to.eq(0)
      expect(await testRewarder.vestingLedger(await wallet1.getAddress())).to.eq(0)
      expect(await rewardToken.balanceOf(await wallet1.getAddress())).to.eq('4438345864661654135')
    })

    it('NFT owner can claim unlocked rewards with vesting and after vesting', async () => {
      await distributor.connect(wallet1).unlockHarvest(nft.address, 2)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 3)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 4)
      await time.increase(86400)
      expect(await rewardToken.balanceOf(await wallet1.getAddress())).to.eq(0)
      // 3.912 before harvest for wallet 1, harvest will add 0.1052
      await distributor.connect(wallet1).harvest(nft.address, 2)
      expect(await rewarder.vestingLedger(await wallet1.getAddress())).to.eq('4017293233082706766') // 4.0172 is 100%
      expect(await rewarder.claimableAmount(await wallet1.getAddress())).to.eq('401731834116541353') // 10% released from 4.01, so ~0.40
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq(0)
      expect(await rewarder.connect(wallet1).claimVested(await wallet1.getAddress()))
      expect(await rewardToken.balanceOf(await wallet1.getAddress())).to.eq('401731973605889724') // 1 second block timestamp potential difference
      expect(await rewarder.claimableAmount(await wallet1.getAddress())).to.eq(0)
      expect(await rewarder.claimedVestedLedger(await wallet1.getAddress())).to.eq('401731973605889724')
      await distributor.connect(wallet1).join(nft.address, 2) // +0.105
      await distributor.updatePool(nft.address) // +0.105
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('315789473684210527') // ~0.315 new reward on NFT, the rest was claimed
      expect(await distributor.pendingRewards(nft.address, 4)).to.eq('8063909774436090225') // ~8.06 reward for passed blocks
      await distributor.connect(wallet1).harvest(nft.address, 2) // +0.105
      expect(await rewarder.connect(wallet1).claimVested(await wallet1.getAddress()))  // +0.105
      expect(await rewardToken.balanceOf(await wallet1.getAddress())).to.eq('443838130978487886') // +10% released from 0.4208, so ~0.4438
      expect(await rewarder.vestingLedger(await wallet1.getAddress())).to.eq('4438345864661654135') // 4.0172 + 0.4208
      await time.increase(86400 * 5 * 30) // roughly 55% should be vested in 5 month // +0.105
      await distributor.updatePool(nft.address) // +0.105
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('315789473684210526') // same reward for 3 blocks
      await distributor.connect(wallet1).harvest(nft.address, 2) // +0.105
      expect(await rewarder.claimableAmount(await wallet1.getAddress())).to.eq('2228835260181704260') // ~55% of a total 4.8588 minus claimed 0.4438
      expect(await rewarder.connect(wallet1).claimVested(await wallet1.getAddress()))  // +0.105
      expect(await rewardToken.balanceOf(await wallet1.getAddress())).to.eq('2672673559889306599') // ~55% of a total 4.8588
      expect(await rewarder.vestingLedger(await wallet1.getAddress())).to.eq('4859398496240601503') // 4.0172 + 0.4208 + 0.4208
      await time.increase(86400 * 10 * 30) // fully vested // +0.105
      expect(await rewarder.connect(wallet1).claimVested(await wallet1.getAddress())) // +0.105
      expect(await rewardToken.balanceOf(await wallet1.getAddress())).to.eq('4859398496240601503')
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('315789473684210526') // +3 blocks
      await distributor.connect(wallet1).harvest(nft.address, 2) // +0.105
      expect(await rewardToken.balanceOf(await wallet1.getAddress())).to.eq('5280451127819548872') // 4.85 + 0.4208
    })

    it('revert if not vested', async () => {
      expect(await rewarder.claimableAmount(await wallet1.getAddress())).to.eq(0)
      await expect(rewarder.connect(wallet1).claimVested(await wallet1.getAddress()))
        .to.be.revertedWithCustomError(rewarder, "NotVested")
    })

    it('only NFT owner can claim vested amount', async () => {
      await distributor.connect(wallet1).unlockHarvest(nft.address, 2)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 3)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 4)
      await time.increase(86400)
      expect(await rewardToken.balanceOf(await wallet1.getAddress())).to.eq(0)
      await distributor.connect(wallet1).harvest(nft.address, 2)
      expect(await rewarder.claimableAmount(await wallet1.getAddress())).to.eq('401731834116541353')
      await expect(rewarder.connect(wallet0).claimVested(await wallet1.getAddress()))
        .to.be.revertedWithCustomError(rewarder, "Unauthorized")
    })

    it('only trusted caller (distributor) can call onReward', async () => {
      await distributor.connect(wallet1).unlockHarvest(nft.address, 2)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 3)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 4)
      await time.increase(86400)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('3912030075187969924')

      await expect(rewarder.connect(wallet1).onReward(
        nft.address,
        2,
        await wallet1.getAddress(),
        await wallet1.getAddress(),
        '3912030075187969924',
        1
      )).to.be.revertedWithCustomError(rewarder, "Unauthorized")
    })

    it('pendingTokens should work', async () => {
      await distributor.connect(wallet1).unlockHarvest(nft.address, 2)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 3)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 4)
      await time.increase(86400)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('3912030075187969924')
      const [[token], [lpAmount]] = await rewarder.pendingTokens(nft.address, 2, 1)
      expect(token).to.eq(rewardToken.address)
      expect(lpAmount).to.eq(1)
    })
  })
})