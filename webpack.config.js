const path = require('path');
const webpack = require('webpack');

module.exports = (env, argv) => { 
  return {
    mode: argv.mode === 'production' ? 'production' : 'development',
    devtool: 'inline-source-map',
    devServer: {
      static: './dist'
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/
        },
      ],
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
      fallback: {
        stream: require.resolve('stream-browserify'),
        assert: require.resolve("assert/")
      }
    },
    entry: {
      example: './src/index.ts',
      rewards_engine: './src/lib.ts'
    },
    output: {
      globalObject: 'this', // for NodeJs support
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].js',
      library: {
        name: 'rewards_engine',
        type: 'umd',
      }
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_DEBUG': false
      })
    ]
  }
}