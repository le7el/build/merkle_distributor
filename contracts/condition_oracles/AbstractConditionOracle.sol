// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165Checker.sol";
import {IConditionOracle} from "../interfaces/IConditionOracle.sol";

/**
 * @dev Shared configuration settings for condition oracles.
 */
abstract contract AbstractConditionOracle is IConditionOracle, Ownable {
    using ERC165Checker for address;

    bytes4 private constant IERC1155_INTERFACE = 0xd9b67a26;
    bytes4 private constant IERC1155_MINT_INTERFACE = 0x731133e9;
    bytes4 private constant IERC20_MINT_INTERFACE = 0x40c10f19;
    bytes4 private constant IERC20_INTERFACE = 0xffffffff;

    bytes32 public integrityHash;
    mapping(address => bool) public override canConsumeClaims;

    event SettingsChanged(
        bytes4 claimInterface,
        address indexed rewardToken,
        uint256 indexed rewardTokenId,
        bytes32 indexed integrityHash
    );

    /**
     * @dev Authorize consumer contract to invalidate claims.
     * @param _consumer Consumer address to authorize / revoke auth.
     * @param _allow true to authorize, false to revoke authorization.
     */
    function adminSwitchConsumer(address _consumer, bool _allow) external onlyOwner {
        if (_allow) {
            canConsumeClaims[_consumer] = true;
            emit ConsumerAdded(_consumer);
        } else {
            canConsumeClaims[_consumer] = false;
            emit ConsumerRemoved(_consumer);
        }
    }

    /**
     * @dev Configure reward engine.
     * @param _rewardToken ERC20 or ERC1155 reward token contract address.
     * @param _rewardTokenId ERC1155 token id used for reward, pass 0 for ERC20 reward token.
     * @param _claimInterface interface used to distribute reward tokens, usually mint or transfer.
     */
    function _changeSettings(bytes4 _claimInterface, address _rewardToken, uint256 _rewardTokenId) internal {
        require(
            _claimInterface == IERC20_INTERFACE
            || _claimInterface == IERC20_MINT_INTERFACE
            || _claimInterface == IERC1155_MINT_INTERFACE
            || _rewardToken.supportsInterface(_claimInterface),
            "ConditionalDistributor: Invalid interface"
        );
        bytes32 _hash = keccak256(abi.encodePacked(_rewardToken, _rewardTokenId, _claimInterface));
        integrityHash = _hash;
        emit SettingsChanged(_claimInterface, _rewardToken, _rewardTokenId, _hash);
    }
}