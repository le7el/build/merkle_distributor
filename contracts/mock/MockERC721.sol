// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

import '@openzeppelin/contracts/token/ERC721/ERC721.sol';

contract MockResolver {
    bytes32 constant public LEVEL1 = keccak256("LEVEL1");
    bytes32 constant public LEVEL2 = keccak256("LEVEL2");
    bytes32 constant public LEVEL5 = keccak256("LEVEL5");
    bytes32 constant public LEVEL10 = keccak256("LEVEL10");

    uint256 public defaultLevel = 1;

    function setDefaultLevel(uint256 _defaultLevel) public {
        defaultLevel = _defaultLevel;
    }

    function level(bytes32 _project, bytes32) public view returns (uint256) {
        if (_project == LEVEL1) {
            return 1;
        } else if (_project == LEVEL2) {
            return 2;
        } else if (_project == LEVEL5) {
            return 5;
        } else if (_project == LEVEL10) {
            return 10;
        } else {
            return defaultLevel;
        }
    }
}

contract MockCRS {
    MockResolver private immutable mockResolver;

    constructor() {
        mockResolver = new MockResolver();
    }

    function resolver(bytes32) public view returns (address) {
        return address(mockResolver);
    }
}

contract MockERC721 is ERC721 {
    constructor (string memory name_, string memory symbol_) ERC721(name_, symbol_) {}

    MockCRS public crs = new MockCRS();
    bytes32 public baseNode = 0x46f49b6a04b289d8ac4920f8a236e64e2689abdcf283566b399ff3bcfb6d967a;

    function mint(address _to, uint _id) public {
        _mint(_to, _id);
    }

    function setDefaultLevel(uint256 _level) public {
        MockResolver(crs.resolver(baseNode)).setDefaultLevel(_level);
    }
}
