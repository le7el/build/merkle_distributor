// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import {ICRS} from "./ICRS.sol";

/**
 * @dev CRS NFT {see https://gitlab.com/le7el/build/crs}.
 */
interface ICrsNft {
    function crs() external view returns (ICRS);
    function baseNode() external view returns (bytes32);
}