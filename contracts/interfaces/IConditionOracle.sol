// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

/**
 * @dev API of conditional oracle.
 */
interface IConditionOracle {
    /**
     * @dev Check if address is an authorized consumer for conditional distributor.
     * @param _consumer Address to check for consumer authorization.
     * @return true if authorized consumer.
     */
    function canConsumeClaims(address _consumer) external view returns (bool);

    /**
     * @dev Check if claim is valid for account.
     * @param _account Account owning the claim.
     * @param _claim ABI encoded claim with integrity hash.
     * @return true if claim is valid for account.
     */
    function hasClaim(address _account, bytes calldata _claim) external view returns (bool);

    /**
     * @dev Use claim, invalidating it in a process.
     *      Can be called only by conditional distributor contract.
     * @param _account Account owning the claim.
     * @param _claim ABI encoded claim with integrity hash.
     * @return reward amount.
     */
    function consumeClaim(address _account, bytes calldata _claim) external returns (uint256);

    // This event is triggered whenever a claim is consumed
    event ConsumedClaim(
        address indexed account,
        bytes claim
    );

    // New consumer added
    event ConsumerAdded(
        address indexed consumer
    );

    // Old consumer removed
    event ConsumerRemoved(
        address indexed consumer
    );
}