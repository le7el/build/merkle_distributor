// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import {IConditionOracle} from "./IConditionOracle.sol";

/**
 * @dev Allows anyone to claim a token based on oracle reporting.
 */
interface IConditionalDistributor {
    /**
     * @dev Check if claim no longer valid with an oracle.
     * @param _oracleContract Address of an authorized oracle contract for the reward claim.
     * @param _claim ABI encoded claim with integrity hash.
     * @return returns true if claim is no longer valid.
     */
    function isClaimed(IConditionOracle _oracleContract, bytes calldata _claim) external view returns (bool);

    /**
     * @dev Claim the given amount of the token to the given address. Reverts if the inputs are invalid.
     * @param _account Address which will get an airdrop.
     * @param _oracleContract Address of oracle which sets the reward amount.
     * @param _claimInterface interface used to distribute reward tokens (mint or transfer selector).
     * @param _rewardToken ERC20 or ERC1155 reward token contract address.
     * @param _rewardTokenId ERC1155 token id used for reward, pass 0 for ERC20 reward token.
     * @param _claim Aidrop claim, check prepareClaim from the oracle for specific claim generation. 
     */
    function claim(
        address _account,
        IConditionOracle _oracleContract,
        bytes4 _claimInterface,
        address _rewardToken,
        uint256 _rewardTokenId,
        bytes calldata _claim
    ) external;

    // This event is triggered whenever a call to #claim succeeds.
    event Claimed(
        address indexed account,
        IConditionOracle indexed oracleContract,
        bytes4 claimInterface,
        address indexed rewardToken,
        uint256 rewardTokenId,
        uint256 amount
    );
}