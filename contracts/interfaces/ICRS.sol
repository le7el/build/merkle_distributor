// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

/**
 * @dev CRS registry {see https://gitlab.com/le7el/build/crs}.
 */
interface ICRS {
    function resolver(bytes32 node) external view returns (address);
}
