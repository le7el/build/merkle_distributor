// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { ethers } = require("hardhat");

const ADMIN = "0xaE992BB0E3359f0d9128ac97B4e3584583D74Fa7" // Polygon multisig
const REWARD_TOKEN_ADDRESS = "0x84a431Bd2C958414B2E316CEdd9F85993ace5000" // "0xF59F671789FB2204b87f6c4405CDf2A6cdA585AA" // Sepolia L7L token
const VIRTUAL_DISTRIBUTOR_ADDRESS = "0x73A699D74734023aE4945FaE6205dfe383347f21" // "0x47D3e90827dFED8Ef5d73dA83D06282e5012d82E" // Sepolia virtual distributor
const AVATAR_NFT_ADDRESS = "0xc477144C19bcaec754109B5b96f2046302850cbC" // "0xc7F81E795FE8eA3242dF960428E54398C32Aadfc" // Sepolia avatar NFT contract
const NOW = Math.round(Date.now() / 1000)
const IN_10_MONTHS = NOW + 86400 * 30 * 10

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  const VestingRewarderContract = await ethers.getContractFactory("VestingRewarder");
  const VestingRewarder = await VestingRewarderContract.deploy(ADMIN);
  await VestingRewarder.deployed();
  await VestingRewarder.immutableConfig(REWARD_TOKEN_ADDRESS, VIRTUAL_DISTRIBUTOR_ADDRESS, NOW, NOW + 1);
  console.info("VestingRewarder deployed to:", VestingRewarder.address);

  const VirtualDistributorContract = await ethers.getContractFactory("VirtualDistributor");
  const VirtualDistributor = VirtualDistributorContract.attach(VIRTUAL_DISTRIBUTOR_ADDRESS);
  await VirtualDistributor.adminSetAllocPoint(AVATAR_NFT_ADDRESS, 1, VestingRewarder.address, true);

  // Verification
  const NETWORK = "live_polygon" //"live_sepolia"
  console.info("To verify your contracts on etherscan run the following commands:")
  console.info(`npx hardhat verify --network ${NETWORK} ${VestingRewarder.address} ${ADMIN}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});