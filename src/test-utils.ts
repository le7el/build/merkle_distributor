type Event = {
  event: Object;
  args: Object[]
};

const emittedEvents : Object[] = []
const logEvents = async (tx: any) => {
    const receipt = await tx.wait()
    receipt.events.forEach((ev: Event) => {
        if (ev.event) {
            emittedEvents.push({
                name: ev.event,
                args: ev.args.map((e) => e.toString())
            });
        }
    });
    console.log(`emittedEvents: `, emittedEvents)
}

export {
    logEvents
}
