import contracts from "./contracts"
import vestingRewarder from "../artifacts/contracts/VestingRewarder.sol/VestingRewarder.json"
import {initContractByAbi} from "./utils"
import {BigNumber, utils, Event} from "ethers"

const ONE_SIDED_STAKING_CONTRACT = 'vesting_rewarder'

const abi = () => {
  return vestingRewarder.abi
}

const bytecode = () => {
  return vestingRewarder.bytecode
}

const deployedAddress = (network : string | number) => {
  if (!contracts[`${network}`]) return null
  return contracts[`${network}`][ONE_SIDED_STAKING_CONTRACT]
}

const initContract = (contractKey : string, readOnly = true, web3Provider = null) => {
  return initContractByAbi(vestingRewarder.abi, contractKey, readOnly, web3Provider)
}

const owner = (web3Provider = null, contractKey = ONE_SIDED_STAKING_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.owner())
}

const vestingStarts = (web3Provider = null, contractKey = ONE_SIDED_STAKING_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.VESTING_STARTS())
}

const vestingEnds = (web3Provider = null, contractKey = ONE_SIDED_STAKING_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.VESTING_ENDS())
}

const vestingLedger = (address : string, web3Provider = null, contractKey = ONE_SIDED_STAKING_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.vestingLedger(address))
}

const claimedVestedLedger = (address : string, web3Provider = null, contractKey = ONE_SIDED_STAKING_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.claimedVestedLedger(address))
}

const claimableAmount = (address : string, web3Provider = null, contractKey = ONE_SIDED_STAKING_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.claimableAmount(address))
}

const claimVested = (address : string, web3Provider = null, contractKey = ONE_SIDED_STAKING_CONTRACT) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.claimVested(address))
}

export {
  abi,
  bytecode,
  deployedAddress,
  owner,
  vestingStarts,
  vestingEnds,
  claimableAmount,
  claimVested,
  vestingLedger,
  claimedVestedLedger
}
export default {
  abi,
  bytecode,
  deployedAddress,
  owner,
  vestingStarts,
  vestingEnds,
  claimableAmount,
  claimVested,
  vestingLedger,
  claimedVestedLedger
}