import contracts from "./contracts"
import distributor from "../artifacts/contracts/ConditionalDistributor.sol/ConditionalDistributor.json"
import {initContractByAbi} from "./utils"
import {BigNumber, utils} from "ethers"

const CONDITIONAL_DISTIBUTOR_CONTRACT = 'conditional_distributor'

const abi = () => {
  return distributor.abi
}

const bytecode = () => {
  return distributor.bytecode
}

const deployedAddress = (network : string | number) => {
  if (!contracts[`${network}`]) return null
  return contracts[`${network}`][CONDITIONAL_DISTIBUTOR_CONTRACT]
}

const initContract = (contractKey : string, readOnly = true, web3Provider = null) => {
  return initContractByAbi(distributor.abi, contractKey, readOnly, web3Provider)
}

const owner = (web3Provider = null, contractKey = CONDITIONAL_DISTIBUTOR_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.owner())
}

const isClaimed = (oracle : string, claim : string, web3Provider = null, contractKey = CONDITIONAL_DISTIBUTOR_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.isClaimed(oracle, claim))
}

const claim = (
  account : string,
  oracle : string, 
  claimInterface : string,
  rewardToken : string,
  rewardTokenId : number | BigNumber,
  claim : string,
  web3Provider = null,
  contractKey = CONDITIONAL_DISTIBUTOR_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.claim(account, oracle, claimInterface, rewardToken, rewardTokenId, claim))
}

const batchedClaims = (
  account : string,
  oracle : string, 
  claimInterface : string,
  rewardToken : string,
  rewardTokenId : number | BigNumber,
  claims : [string],
  web3Provider = null,
  contractKey = CONDITIONAL_DISTIBUTOR_CONTRACT
) => {
  let inter = new utils.Interface(abi());
  const calls = claims.map((claim) => inter.encodeFunctionData(
    "claim",
    [account, oracle, claimInterface, rewardToken, rewardTokenId, claim]
  ))
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.multicall(calls))
}

export {
  abi,
  bytecode,
  deployedAddress,
  owner,
  isClaimed,
  claim,
  batchedClaims
}
export default {
  abi,
  bytecode,
  deployedAddress,
  owner,
  isClaimed,
  claim,
  batchedClaims
}